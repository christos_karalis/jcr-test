package com.jcn.service;

import javax.ejb.Remote;
import javax.jms.JMSException;

/**
 * Created by Χρήστος on 10/3/2014.
 */
@Remote
public interface MessageWriter {

    public void createMessage(String message) throws JMSException;
}
