package com.jcn.service;

import javax.ejb.Remote;

/**
 * Created by Χρήστος on 9/3/2014.
 */
@Remote
public interface DocumentService {

    /**
     *
     * @param nodeCounter
     */
    void uploadDocument(String nodeCounter);

    /**
     *
     * @param nodeCounter
     */
    String uploadDocumentAndPropagateMessage(String nodeCounter);

}
