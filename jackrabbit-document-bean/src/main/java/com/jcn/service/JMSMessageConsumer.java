package com.jcn.service;

import org.apache.log4j.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * Created by Χρήστος on 10/3/2014.
 */
@MessageDriven(activationConfig =  {
        @ActivationConfigProperty(propertyName = "acknowledgeMode",
                propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType",
                propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination",
                propertyValue = "queue/test")
})
public class JMSMessageConsumer implements MessageListener {

    public static final Logger LOGGER = Logger.getLogger(JMSMessageConsumer.class);

    @Override
    public void onMessage(Message message) {

        try {
            LOGGER.debug("Read message : " + ((MapMessage) message).getString("message"));
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
