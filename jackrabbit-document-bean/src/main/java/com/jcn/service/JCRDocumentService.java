package com.jcn.service;

import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jcr.*;
import javax.jms.JMSException;
import java.util.Date;

/**
 * Created by Χρήστος on 9/3/2014.
 */
@Stateless
public class JCRDocumentService implements DocumentService {

    public static final Logger LOGGER = Logger.getLogger(JCRDocumentService.class);

    @Resource(name = "java:/jcrJCAXA")
    private Repository repository;

    @EJB
    private MessageWriter messageWriter;

    @EJB
    private DocumentService documentService;

    @Override
    public void uploadDocument(String nodeCounter) {
        Session session = null;
        try {
            LOGGER.debug("+++++++++++++++++++++++++++++++++++++++++++++++");
            session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
            Node node = getOrAddNode(session.getRootNode(), nodeCounter, "nt:folder");
            LOGGER.debug("Added node :" + node.getName() + ":" + node.getPrimaryNodeType().getName());
            LOGGER.debug("+++++++++++++++++++++++++++++++++++++++++++++++");

        } catch (RepositoryException e) {
            e.printStackTrace();
        } finally {
            if (session!=null) {
                try {
                    session.save();
                } catch (RepositoryException e) {
                    //for testing reasons just prints the stack trace
                    e.printStackTrace();
                }
                session.logout();
            }
        }
    }

    @Override
    public String uploadDocumentAndPropagateMessage(String nodeCounter) {
        String documentNode = nodeCounter + "_" + new Date().getTime();
        documentService.uploadDocument(documentNode);

        try {
            messageWriter.createMessage(documentNode);
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        return documentNode;
    }


    private Node getOrAddNode(Node parent, String name, String type)
            throws RepositoryException {
        if (parent.hasNode(name)) {
            return parent.getNode(name);
        } else if (type != null) {
            return parent.addNode(name, type);
        } else {
            return parent.addNode(name);
        }
    }
}
