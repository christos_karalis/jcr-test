package com.jcn.service;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;

/**
 * Created by Χρήστος on 10/3/2014.
 */
@Stateless
public class JMSMessageWriter implements MessageWriter {

    @Resource(name = "java:/JmsXA")
    private ConnectionFactory connectionFactory;

    @Override
    public void createMessage(String message) {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue("testQueue");
            MapMessage mapMessage = session.createMapMessage();

            mapMessage.setString("message", message);

            session.createProducer(queue).send(mapMessage);

            if (message!=null
                    && message.indexOf("fail")>-1) {
                throw new RuntimeException();
            }
        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            if (connection!=null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }

    }


}
