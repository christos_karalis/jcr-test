package com.jcn.jackrabbit.beans;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

/**
 * Created by Χρήστος on 11/3/2014.
 */
@RunWith(Arquillian.class)
public class DocumentBeanIT {

    @Deployment
    public static Archive<?> createDeployment() {
        // or jar packaging...
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
            .addPackage(DocumentBean.class.getPackage())

            .addAsManifestResource("test-beans.xml", "beans.xml");

        return jar;
    }

    @Inject
    DocumentBean documentBean;

    @Test
    public void testBean() {
        documentBean.callDocument();
    }

}
