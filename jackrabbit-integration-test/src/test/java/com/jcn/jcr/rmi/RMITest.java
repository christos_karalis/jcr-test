package com.jcn.jcr.rmi;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.rmi.repository.RmiRepositoryFactory;
import org.junit.Test;

import javax.jcr.*;
import java.util.*;

/**
 * Created by Χρήστος on 9/3/2014.
 */
public class RMITest {

    @Test
    public void testRMI() throws RepositoryException {
        String rmiUrl = System.getProperty("jcr.rmi.address");

        Map parameters = new HashMap();
        parameters.put("org.apache.jackrabbit.repository.uri", rmiUrl);

        Session session = null;
        try {
            Repository repository = JcrUtils.getRepository(parameters);

            session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
            JcrUtils.getOrAddNode(session.getRootNode(), "rmitest_" + new Date().getTime(), "nt:folder");

            System.out.println(repository);

            System.out.println(session);
        } catch (RepositoryException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.save();
                session.logout();
            }

        }

    }

}
