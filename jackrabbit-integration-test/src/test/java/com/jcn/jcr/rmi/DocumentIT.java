package com.jcn.jcr.rmi;

import com.jcn.service.DocumentService;
import com.jcn.service.MessageWriter;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.rmi.repository.RmiRepositoryFactory;
import org.junit.Assert;
import org.junit.Test;

import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Χρήστος on 9/3/2014.
 */
public class DocumentIT {

    private Object lookup(String ejb) throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);

        return context.lookup(ejb);
    }

    @Test
    public void testRemoteClient() throws NamingException {
        String ejb = "ejb:jackrabbit-document-ear/jackrabbit-document-bean-1.0-SNAPSHOT//JCRDocumentService!com.jcn.service.DocumentService";

        DocumentService documentService = (DocumentService) lookup(ejb);
        System.out.println("===============================================");
        documentService.uploadDocument("testNode_" + new Date().getTime());
        System.out.println("===============================================");

    }

    @Test
    public void testMessageQueue() throws JMSException, NamingException {
        String ejb = "ejb:jackrabbit-document-ear/jackrabbit-document-bean-1.0-SNAPSHOT//JMSMessageWriter!com.jcn.service.MessageWriter";

        MessageWriter messageWriter = (MessageWriter) lookup(ejb);
        System.out.println("===============================================");
        messageWriter.createMessage("testMessage_" + new Date().getTime());
        System.out.println("===============================================");

    }


    @Test
    public void testJCR_JMS() throws NamingException, RepositoryException {
        String ejb = "ejb:jackrabbit-document-ear/jackrabbit-document-bean-1.0-SNAPSHOT//JCRDocumentService!com.jcn.service.DocumentService";

        DocumentService documentService = (DocumentService) lookup(ejb);
        System.out.println("===============================================");
        String testNode = documentService.uploadDocumentAndPropagateMessage("testNode");
        System.out.println("===============================================");

        Assert.assertTrue(checkNode(testNode));
    }

    @Test
    public void testJCR_JMS_failed() throws NamingException, RepositoryException {
        String ejb = "ejb:jackrabbit-document-ear/jackrabbit-document-bean-1.0-SNAPSHOT//JCRDocumentService!com.jcn.service.DocumentService";

        DocumentService documentService = (DocumentService) lookup(ejb);
        System.out.println("===============================================");
        String failed_Nodes = documentService.uploadDocumentAndPropagateMessage("failedNode");
        System.out.println("===============================================");

        Assert.assertFalse(checkNode(failed_Nodes));

    }

    public boolean checkNode(String documentNode) throws RepositoryException {
        String rmiUrl = System.getProperty("jcr.rmi.address");

        Map parameters = new HashMap();
        parameters.put("org.apache.jackrabbit.repository.uri", rmiUrl);
        RmiRepositoryFactory rmiRemoteRepositoryFactory = new RmiRepositoryFactory();
        Session session = null;
        try {
            Repository repository = JcrUtils.getRepository(parameters);

            session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
            return (JcrUtils.getNodeIfExists(session.getRootNode(), documentNode)==null?false:true);

        } catch (RepositoryException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (session != null) {
                session.save();
                session.logout();
            }

        }
    }

}
